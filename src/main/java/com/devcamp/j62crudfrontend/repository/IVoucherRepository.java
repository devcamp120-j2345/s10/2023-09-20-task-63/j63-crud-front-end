package com.devcamp.j62crudfrontend.repository;

import com.devcamp.j62crudfrontend.model.CVoucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}
